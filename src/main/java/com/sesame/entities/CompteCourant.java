/**
 * 
 */
package com.sesame.entities;

import java.util.Date;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


@Entity
@DiscriminatorValue("CC")
public class CompteCourant extends Compte {



	public CompteCourant() {
		super();

	}

	public CompteCourant(String codeCompte, Date dateCreation, double solde, Client client) {
		super(codeCompte, dateCreation, solde, client);

	}



}
