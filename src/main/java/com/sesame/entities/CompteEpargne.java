/**
 * 
 */
package com.sesame.entities;

import java.util.Date;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


@Entity
@DiscriminatorValue("CE")
public class CompteEpargne extends Compte {



	public CompteEpargne() {
		super();

	}

	public CompteEpargne(String codeCompte, Date dateCreation, double solde, Client client) {
		super(codeCompte, dateCreation, solde, client);

	}

	
}
