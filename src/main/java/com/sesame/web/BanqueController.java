/**
 * 
 */
package com.sesame.web;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.sesame.dao.ClientRepository;
import com.sesame.dao.CompteRepository;
import com.sesame.entities.Client;
import com.sesame.entities.Compte;
import com.sesame.entities.CompteCourant;
import com.sesame.entities.CompteEpargne;
import com.sesame.entities.Operation;
import com.sesame.metier.IBanqueMetier;

@Controller
public class BanqueController {

	@Autowired
	private IBanqueMetier banqueMetier;
	@Autowired
	private ClientRepository clientRepository;
	@Autowired 
	private CompteRepository compteRepository;
	
	@RequestMapping(value= {"/","index"},method=RequestMethod.GET)
	public String index() {
		
		return "comptes";
	}
	
	@RequestMapping(value="/consulterCompte")
	public String consulter(Model model, String codeCompte,
			@RequestParam(name="page", defaultValue="0")int page,
			@RequestParam(name="size",defaultValue="5")int size) {

		try {
			Compte compte = banqueMetier.consulterCompte(codeCompte);
			model.addAttribute("compte", compte);
			
			Page<Operation> pageOperations = banqueMetier.listOperation(codeCompte, page, size);
			model.addAttribute("listOperations", pageOperations.getContent());
			int[] pages = new int[pageOperations.getTotalPages()];
			model.addAttribute("pages",pages);
			model.addAttribute("pageCourante", page);
			
		}catch(Exception e) {
			model.addAttribute("exception", e);
		}
		
		return "comptes";
	}
	
	@RequestMapping(value="/saveOperation",method=RequestMethod.POST)
	public String saveOperation(Model model,String typeOperation,String codeCompte,double montant,String codeCompte2) {
		try {
			if(typeOperation.equals("versement")) {
				banqueMetier.verser(codeCompte, montant);
			}
			else if(typeOperation.equals("retrait")) {
				banqueMetier.retirer(codeCompte, montant);
			}
			else {
				banqueMetier.virement(codeCompte, codeCompte2, montant);
			}
		}catch(Exception e) {
			model.addAttribute("error", e);
			return "redirect:/consulterCompte?codeCompte="+codeCompte+"&error="+e.getMessage();
		}
		
		return "redirect:/consulterCompte?codeCompte="+codeCompte;
	}
	
	@RequestMapping(value="/nouveau")
	String creerCompte() {
		
		return "creerCompte";
	}
	
	@RequestMapping(value="/create",method=RequestMethod.POST)
	public String createNewAccount(String codeCompte, String nom,  String email, String typeCompte, String solde) {
		double _solde = Double.parseDouble(solde);
		
		Client client=clientRepository.save(new Client(nom,email));
		System.out.println("=============::"+typeCompte+"========::=======");

		return "redirect:/";
	}
}
