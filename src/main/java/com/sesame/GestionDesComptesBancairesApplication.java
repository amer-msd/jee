package com.sesame;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.sesame.dao.ClientRepository;
import com.sesame.dao.CompteRepository;
import com.sesame.dao.OperationRepository;
import com.sesame.metier.IBanqueMetier;

@SpringBootApplication
public class GestionDesComptesBancairesApplication implements CommandLineRunner {

	@Autowired
	private ClientRepository clientRepository;
	@Autowired
	private CompteRepository compteReposiory;
	@Autowired
	private OperationRepository operationRepository;
	@Autowired
	private IBanqueMetier banqueMetier;
	
	
	public static void main(String[] args) {
		
		SpringApplication.run(GestionDesComptesBancairesApplication.class, args);
		
	}

	@Override
	public void run(String... args) throws Exception {



	}

}
