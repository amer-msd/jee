/**
 * 
 */
package com.sesame.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sesame.entities.Client;


public interface ClientRepository extends JpaRepository<Client, Long> {

}
